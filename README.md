# xv6-std

This project is a [limited] C++ standard library for the XV6 operating system.

It intends, as XV6, to be simple and understandable by students, in order to teach them computer science. As it, only the basic functionalities are coded, so others can be used as exercises for their professors.
